package main;

public class App {
	public static void main(String[] args) {
		Motor motor = new Motor();
		Visor visor = new Visor();
		Calculador calculador = new Calculador();
		
		int[] sorteios = new int[6];
		
		for(int i = 0; i < sorteios.length; i++) {
			sorteios[i] = motor.sortear();
		}
		
		int pontos = calculador.calcular(sorteios);
		
		visor.imprimir(sorteios, pontos);
	}
}
