package aula;

public enum Segmento {
	POBRITE("Comum", 1), 
	UNICLASS("Uniclass", 2), 
	PERSONALITE("Perssonnallitté", 3), 
	PRIVATE("Sílvio Santos", 4);
	
	private String descricao;
	private int codigo;
	
	private Segmento(String descricao, int codigo) {
		this.descricao = descricao;
		this.codigo = codigo;
	}
	
	public static Segmento findByCodigo(int codigo) {
		for(Segmento segmento : Segmento.values()) {
			if(segmento.getCodigo() == codigo) {
				return segmento;
			}
		}
		
		return null;
	}
	
	public String toString() {
		return descricao;
	}
	
	public String getDescricao() {
		return descricao;
	}
	
	public int getCodigo() {
		return codigo;
	}
}
