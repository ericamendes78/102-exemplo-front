package aula;

public class App {
	
	public static void main(String[] args) {
		Cliente cliente = new Cliente();
		
		cliente.nome = "Renan Rodrigo";
		cliente.cpf = "12312312";
		cliente.conta = new Conta();
		cliente.conta.segmento = Segmento.UNICLASS;
		cliente.angustias = new Angustias[10];
		
		cliente.angustias[0] = Angustias.DIVIDAS;
		cliente.angustias[1] = Angustias.DIVIDAS;
		cliente.angustias[2] = Angustias.DIVIDAS;
		cliente.angustias[3] = Angustias.DIVIDAS;
		cliente.angustias[4] = Angustias.DIVIDAS;
		cliente.angustias[5] = Angustias.DIVIDAS;
		cliente.angustias[6] = Angustias.CHOPP_AZEDO;
		
		Funcionario funcionario = new Funcionario();
		
		funcionario.nome = "Renan";
		funcionario.cpf = "657756567756";
		funcionario.userId = "rxpto123";
		funcionario.emailCorporativo = "renan@itau-unibanco.com.br";
		
		imprimirPessoa(cliente);
		imprimirPessoa(funcionario);
	}
	
	public static void imprimirPessoa(Pessoa pessoa) {
		System.out.println(pessoa);
	}
	
}








