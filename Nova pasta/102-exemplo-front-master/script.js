//definição das variáveis globais

const pagina = document.querySelector("body");
const formulario = document.querySelector("form");
const links = document.querySelectorAll("a");
const inputEmail = document.querySelector("#email");
const inputSenha = document.querySelector("#senha");
const tempo = document.querySelector("marquee");

let cor = "blue";

// definição das funções

function validarEmail(){
    let email = inputEmail.value;
    let arroba = email.indexOf("@");
    let ultimoArroba = email.lastIndexOf("@");
    let ultimoPonto = email.lastIndexOf(".");

    if((arroba === ultimoArroba && arroba !== -1 &&
       ultimoPonto - arroba >= 3) || email === ""){
           return true;
       }
       return false;
}

function temLetra(texto){
    for(let caracter of texto){
        let code = caracter.charCodeAt(0);
        if((code > 64 && code < 91) ||
           (code > 96 && code < 123)){
               return true;
           }
    }
    return false;
}

function temNumero(texto){
    for(let caracter of texto){
        let code = caracter.charCodeAt(0);
        if(code > 47 && code < 58){
               return true;
           }
    }
    return false;
}

function validarSenha(){
    let senha = inputSenha.value;

    if(senha.length >= 6 && temLetra(senha) &&
       temNumero(senha)){
           return true;
    }
    return false;   
}

function eventoEmail(){
    if(validarEmail()){
        document.querySelector("#label-email").style.display = "none";
    }
    else{
        document.querySelector("#label-email").style.display = "block";
    }
}

function eventoSenha(){
    if(validarSenha()){
        document.querySelector("#label-senha").style.display = "none";
    }
    else{
        document.querySelector("#label-senha").style.display = "block";
    }
}

function linkAzuis(){
    for(let link of links){
        link.style.color = cor;
    }

    if(cor === "blue"){
        cor = "white";
    }
    else{
        cor = "blue";
    }
}

function crescer(){
    this.style.padding = "60px 60px";
}

function girar(){
    this.style.transform = "rotate(180deg)";
}

function desgirar(){
    this.style.transform = "rotate(0deg)";
}

// atribuções dos eventos

pagina.onclick = linkAzuis;
formulario.onmouseenter = crescer;
for(let link of links){
    link.onmouseenter = girar;
    link.onmouseleave = desgirar;
}

inputEmail.onblur = eventoEmail;
inputSenha.onblur = eventoSenha;

//timeouts e intervals

function contarTempo(){
    tempo.innerHTML = Number(tempo.innerHTML) - 1;
    if(Number(tempo.innerHTML) < 1){
        alert("Tempo para login esgotado");
        window.location = "http://heeeeeeeey.com/";
        //clearInterval(contador);
    }
}

let contador = setInterval(contarTempo, 1000);