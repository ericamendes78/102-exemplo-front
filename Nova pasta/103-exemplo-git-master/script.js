let idBaralho;
const botaoIniciar = document.querySelector("#iniciar");
const botaoCarta = document.querySelector("#carta");
const botaoParar = document.querySelector("#parar");
const mesa = document.querySelector("#mesa");
let pontos = document.querySelector("#pontos");
const msg = document.querySelector("#acoes p");

function extrairJSON(resposta){
    return resposta.json();
}

function atualizarPlacar(valor){
    let total = Number(pontos.innerHTML) + valor;

    if(total === 21){
        msg.innerHTML = "Você Venceu! Parabéns!";
        botaoIniciar.onclick = iniciarJogo;
        botaoCarta.onclick = null;
        botaoParar.onclick = null;
    }
    else if(total > 21){
        msg.innerHTML = "Você Perdeu! :( ";
        botaoIniciar.onclick = iniciarJogo;
        botaoCarta.onclick = null;
        botaoParar.onclick = null;
    }
    else{
        pontos.innerHTML = total;
    }

}

function desenharCarta(carta){
    let desenho = document.createElement("img");
    desenho.src = carta.image;
    desenho.alt = carta.code;

    mesa.appendChild(desenho);

    let valor;
    if(carta.value === "ACE"){
        valor = 1;
    }
    else if(carta.value === "QUEEN" || carta.value === "KING" || carta.value === "JACK"){
        valor = 10;
    }
    else{
        valor = Number(carta.value);
    }

    atualizarPlacar(valor);
}

function preencherMesa(dados){
    let cartas = dados.cards;
    for (let carta of cartas){
        desenharCarta(carta);
    }
}

function iniciarMesa(dados){
    idBaralho = dados.deck_id;
    fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=2`).then(extrairJSON).then(preencherMesa);
}

function novaCarta(){
    fetch(`https://deckofcardsapi.com/api/deck/${idBaralho}/draw/?count=1`).then(extrairJSON).then(preencherMesa);
}

function pararJogo(){
    msg.innerHTML = `Você parou com ${pontos.innerHTML} pontos.`;
    botaoIniciar.onclick = iniciarJogo;
    botaoCarta.onclick = null;
    botaoParar.onclick = null;
}

function iniciarJogo(){
    mesa.innerHTML = "";
    msg.innerHTML = "Pontuação: <span id=\"pontos\">0</span>";
    pontos = document.querySelector("#pontos");
    fetch("https://deckofcardsapi.com/api/deck/new/shuffle/").then(extrairJSON).then(iniciarMesa);
    botaoIniciar.onclick = null;
    botaoCarta.onclick = novaCarta;
    botaoParar.onclick = pararJogo;
}

botaoIniciar.onclick = iniciarJogo;