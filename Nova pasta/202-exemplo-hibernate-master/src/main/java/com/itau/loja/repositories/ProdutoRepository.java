package com.itau.loja.repositories;

import org.springframework.data.repository.CrudRepository;

import com.itau.loja.models.Produto;

public interface ProdutoRepository extends CrudRepository<Produto, Long>{
	
}
