package aula;

import java.io.IOException;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.Scanner;

public class App {
	
	public static void main(String[] args) {
//		System.out.println("Hello Itaú");
//		
////		Tipos de Variável
//		int numero = 10;
//		float numeroDecimal = 1.5f;
//		double numeroDecimal2 = 40.1;
//		char umCaractere = 'a';
//		boolean eVerdadeiro = false;
//		String umaFrase = "Olar!";
//		
//		System.out.println(numero);
//		
////		Condicionais
//		if(numero == 10) {
//			System.out.println(numero);
//		}else if(numero == 20) {
//			System.out.println("Não vai entrar aqui");
//		}else {
//			System.out.println("Nem aqui");
//		}
//		
////		Laços
//		for(int i = 0; i < 5; i ++) {
//			System.out.println(i);
//		}
//		
//		int i = 0;
//		while(i < 5) {
//			System.out.println(i);
//			i++;
//		}
//		
////		Vetores
//		int[] numeros = {1, 2, 3};
//		int[] novosNumeros = new int[10];
//		String[] palavras = {"celular", "batata", "frango"};
//		
//		System.out.println(numeros[2]);
//		novosNumeros[5] = 30;
//		
//		for(String palavra : palavras) {
//			System.out.println(palavra);
//		}
//		
////		Scanner
//		Scanner scanner = new Scanner(System.in);
////		int numeroEscolhido = scanner.nextInt();
//		String dadosUsuario = scanner.nextLine();
//		
//		try {
//			int numeroConvertido = Integer.parseInt(dadosUsuario);
//			System.out.println(numeroConvertido + 1);
//		} catch (Exception e) {
//			System.out.println("Não foi possível converter o número");
//		}

//		Cria e lança uma excessão
//		throw new Exception("Exemplo");
		
//		System.out.println("Fim");
//		scanner.close();
		
//		Scanner scanner = new Scanner(System.in);
//		
//		String st1 = scanner.nextLine();
//		String st2 = scanner.nextLine();
//		
//		System.out.println(st1.equals(st2));
		
//		Exemplo de uso de Math
//		double sorteio = Math.random();
//		sorteio = Math.ceil(sorteio * 60);
//		System.out.println(sorteio);
		
//		Exemplo de gravação de arquivo
		Path path = Paths.get("exemplo.txt");
		String umaString = "Vou gravar um arquivo";
		
		try {
			Files.write(path, umaString.getBytes());
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		}
		
		String[] partes = umaString.split(" ");
		
		System.out.println(partes[0]);
	}
	
}








